# Rewrite and Theorem Proving

One of the great things about idris is how much information you can encode in the
type of values. This allows you to express powerful things like linearity and
relations between function arguments. But it also means that you need to
sometimes prove to the compiler that you are able to call a certain function.

## A quick overview of Vect

We can see this with the
[`Vect`](https://www.idris-lang.org/docs/idris2/current/base_docs/docs/Data.Vect.html)
type and the functions that manipulate it:

As a quick reminder:

```ignore idris
data Nat = Z | S Nat

data Vect : Nat -> Type -> Type where
    Nil : Vect Z t
    (::) : t -> Vect len t -> Vect (S len) t
```

We can already see that `Vect` append (`::`) has some interesting information
attached to it; when we append to a Vector it's length increases!

Let's look at some other functions:

```ignore idris
tail : Vect (S len) t -> Vect len t
head : Vect (S len) t -> t
```

Look at that! Both `head` and `tail` cannot be called on empty Vectors (`Vect Z
elem`)! This is not a suggestion, the idris compiler will enforce that no call
to `head` nor `tail` will compile, if the `Vect` it is provided with doesn't
have at least one element.

Let's try calling it:

```ignore idris
hasATwo : Vect len Nat -> Bool
hasATwo xs = if head xs == 2 then True
			     else hasATwo $ tail xs
```

This is missing a default case and can be done much easier with pattern matching
or by using the `find` function. The important thing is, it won't compile!

### A practical example (featuring proofs)

Let's take a look at a more practical example:

```ignore idris 
windows : (n : Nat) -> Vect len t -> List (Vect n t)
```

Here we want to get a window of size `n` on adjacent elements, for example:

```ignore idris
windows 3 [1..5] == [[1, 2, 3], [2, 3, 4], [3, 4, 5]]
```

The signature of `windows` currently tells us a small amount of information
about the function itself. We can add more constraints; we know how many windows
we must return, so we can instead return a `Vect` saying exactly how many windows
that'll be:

```ignore idris 
windows : (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect (n) t)
```

And naive implementation would be:

```ignore idris 
windows 0 = replicate (S m) []
windows n = ?todo
```

But this will not compile:

```
Error: While processing right hand side of windows. m is not accessible in this context.

    | windows : (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect (n) t)
    | windows 0 = replicate (S m) []
                               ^
```

This is because `m` is not a parameter of the function. In `idirs1` `m` would be
passed implicitely, you need to be explicit about this in the signature of the
function.

You can see what is available to you by placing a hole in the body of the
function, then you can ask your editor to show you what is available to the
hole. In Vim for example you can use the lsp hover action to check (`:lua
vim.lsp.buf.hover()`).

```ignore idris 
windows : { m : Nat } -> (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows 0 _ = ?zero_case
windows n v = ?non_zero_case
```

Here the hover dialog of the `?zero_case` hole is:

```
 0 t : Type
   m : Nat
------------------------------
zero_case : Vect (S m) (Vect 0 t)
```

And the `?non_zero_case`

```
 0 t : Type
   m : Nat
   n : Nat
   v : Vect (plus n m) t
------------------------------
non_zero_case : Vect (S m) (Vect n t)
```

The hover menu has two parts delimited by a dashed line (`---`). Above the line
you will find the things available in the current scope with their respective
types after the colon (`:`). For example `m` is a `Nat` and `v` is a `Vect (plus
n m) t`. A special case is `t`, `t` has a
[multiplicity](https://idris2.readthedocs.io/en/latest/tutorial/multiplicities.html)
of `0`, multiplicities are out of scope for this part of the tutorial, but a
quick rule of thumb is, if something has a multiplicity of `0`, it cannot be
named at runtime (in the body of the function).

Here lays the problem with the previous code, if we change the signature and
inspect a hole this is what well see:


```ignore idris 
windows : (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows n v = ?body
```

```
 0 t : Type
 0 m : Nat
   n : Nat
   v : Vect (plus n m) t
------------------------------
body : Vect (S m) (Vect n t)
```

Because `m` is not an argument to the function, just like `t` it gets a
multiplicity of `0`. `{ m : Nat }` explicitely sets the multiplicity of `m` to
an unrestricted value, which allows us to mention it inside the body.

Let's complete the function now:

<!-- idris
import Data.Vect
-->

```ignore idris
windows : { m : Nat } -> (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows 0 _ = replicate (S m) []
windows n v = take n v :: (windows n $ tail v)
```

Ah! We forgot a case:

```
Error: While processing right hand side of windows. When unifying:
    Vect (S (S ?m)) (Vect n t)
and:
    Vect (S m) (Vect n t)
Mismatch between: S ?m and m.
```

It is telling us that it cannot prove that `(S ?m)` and `m` are the same,
because they are not! We missed a base case for when `m = 0`! This is thanks to
our very specific signature.

```ignore idris
windows : { m : Nat } -> (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows 0 _ = replicate (S m) []
windows {m=0} n v = [v]
windows {m=S k} n v = ?body
```

We get an error? What is that?

```
Error: While processing right hand side of windows. Can't solve constraint between: plus n 0 and n.
```

`plus n 0` and `n` are not equal? What is wrong with you idris? Nothing in fact!
`plus n 0` and `n` are the same! And we have proof of it! Just look at
[`Data.Nat`](https://www.idris-lang.org/docs/idris2/current/base_docs/docs/Data.Nat.html)
there we have a bunch of common proofs, specifically
[`plusZeroRightNeutral`](https://www.idris-lang.org/docs/idris2/current/base_docs/docs/Data.Nat.html#Data.Nat.plusZeroRightNeutral).
This proves that `plus x 0 = x` (zero is neutral). We can use this proof to tell
idris that `Vect (plus n 0) t` is infact the same as `Vect n t` like so:

```ignore idris
import Data.Nat

windows : { m : Nat } -> (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows 0 _ = replicate (S m) []
windows {m=0} n v = rewrite sym $ plusZeroRightNeutral n in [v]
windows {m=S k} n v = ?body
```

`rewrite` takes a proof (`expr = expr'`) and replaces all ocurences of `expr`
with `expr'`. We can use `plusZeroRightNeutral n` to show that `plus n 0 = n`,
but this does not work, as `v` is of type `Vect n t`, thus we cannot replace any
ocurrences of `plus n 0` with `n`. What we want is a proof of `n = plus n 0`,
thankfully because `=` is reflexive any proof of `a = b` is also a proof of `b =
a`, we just need to coerce it a little bit with `sym`.

Let's move on to writing the body:

<!-- idris
import Data.Nat
-->

```ignore idris
windows : { m : Nat } -> (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows 0 _ = replicate (S m) []
windows {m=0} n v = rewrite sym $ plusZeroRightNeutral n in [v]
windows {m=S k} n v = take n v :: windows n (tail v)
```

Another error? Let's take a quick look at it:

```
Error: While processing right hand side of windows. Can't solve constraint between: plus n (S k) and S (n + k).
```

So idris also can't prove that `n + (k + 1) = (n + k) + 1` huh, do we have
anothoer proof in `Data.Nat`? Of course we do! It is
[`plusSuccRightSucc`](https://www.idris-lang.org/docs/idris2/current/base_docs/docs/Data.Nat.html#Data.Nat.plusSuccRightSucc).

Let's quickly look at the type of the hole:

```ignore idris
windows {m=S k} n v = take n v :: windows n (tail ?hole)
```

```
 0 t : Type
   k : Nat
   n : Nat
   v : Vect (plus n (S k)) t
------------------------------
hole : Vect (S (plus n k)) t
```

And seeing that `plusSuccRightSucc` proves that
`S (left + right) = left + S right` we wont need to flip the equality to rewrite
the hole:

```idris
windows : { m : Nat } -> (n : Nat) -> Vect (n + m) t -> Vect (S m) (Vect n t)
windows 0 _ = replicate (S m) []
windows {m=0} n v = rewrite sym $ plusZeroRightNeutral n in [v]
windows {m=S k} n v = take n v :: windows n (tail (rewrite plusSuccRightSucc n k in v))
```

It compiles! And the best part is; we don't need to test if it will return the
right number of elements, the compiler already checked for us!

## Theorem proving (writing proofs for fun and profit!)

Let's do a little bit of theorem proving around `Nat`s to get our feet wet. A
quick reminder:

```ignore idris
data Nat = Z | S Nat

plus : Nat -> Nat -> Nat
plus Z y     = y
plus (S x) y = plus x (S y)
```

### Proving Equalities

Some proofs are trivial:

```idris
plusZeroLeftNeutral : (right : Nat) -> 0 + right = right
```

The definition of plus gives us this equality (`plus Z y = y`), but we still
need to give idris a proof. The simplest one is `x = x`, also called `Refl`, the
reflexive property of equality (`=`) which is a builtin proof (axiom).

This lets us prove the above statement by saying it is trivial:

```idris
plusZeroLeftNeutral right = Refl
```

### Proving statements by induction

The proof we used before is not as simple:

```idris
plusZeroRightNeutral : (left : Nat) -> left + 0 = left
```

`plus` itself does not say anything about the right, but we can easily do a
proof by induction:

<!-- idris
%hide Data.Nat.plusZeroRightNeutral
-->

```idris
plusZeroRightNeutral 0        = Refl
plusZeroRightNeutral (S left) = cong S (plusZeroRightNeutral left)
```

`cong` is simply the proof that `f a = f b` so long as `a = b` (this is called
congruence which is a property of equality). Because we can prove that `0 + 0 =
0`, we can then use that as an induction case on the proof.

### Rewrite all the things!

Let's try proving something more complex:

```idris
complexExample : (x, y, z : Nat) -> (1 + x) + (y + z) = (x + z) + S y
```

There is nothing that directly gives us the proof and induction will be very
verbose on this (I don't want to try it). So let's go back to our old friend
rewrite! First, let's check the hole:

```ignore idris
complexExample x y z = ?hole
```

```
   z : Nat
   y : Nat
   x : Nat
------------------------------
hole : S (plus x (plus y z)) = plus (plus x z) (S y)
```

Let's first try to form that `plus x z` on the left:

```ignore idris
complexExample x y z =
    rewrite plusCommutative y z -- plus y z = plus z y
    in ?hole
```

```
   z : Nat
   y : Nat
   x : Nat
------------------------------
hole : S (plus x (plus z y)) = plus (plus x z) (S y)
```

```ignore idris
complexExample x y z =
    rewrite plusCommutative y z      -- plus y z          = plus z y
    in rewrite plusAssociative x z y -- plus x (plus z y) = plus (plus x z) y
    in ?hole
```

```
   z : Nat
   y : Nat
   x : Nat
------------------------------
hole : S (plus (plus x z) y) = plus (plus x z) (S y)
```

> Take note that the `rewrite`s are evaluated from the outside to the inside, so
> first `plusCommutative` will be applied, then `plusAssociative`. In the same
> order they appear.


```ignore idris
complexExample x y z =
    rewrite plusCommutative y z             -- plus y z           = plus z y
    in rewrite plusAssociative x z y        -- plus x (plus z y)  = plus (plus x z) y
    in rewrite plusSuccRightSucc (x + z) y  -- S (plus (x + z) y) = plus (x + z) (S y)
    in ?hole
```

```
   z : Nat
   y : Nat
   x : Nat
------------------------------
hole : plus (plus x z) (S y) = plus (plus x z) (S y)
```

We have successfully rewritten both sides to be the same. Now we can fall back
to `Refl` to prove they are equal!

```idris
complexExample x y z =
    rewrite plusCommutative y z             -- plus y z           = plus z y
    in rewrite plusAssociative x z y        -- plus x (plus z y)  = plus (plus x z) y
    in rewrite plusSuccRightSucc (x + z) y  -- S (plus (x + z) y) = plus (x + z) (S y)
    in Refl
```

Try taking a look at the proofs in `Data.Nat` and see if you can rewrite other
proofs (make sure to use `%hide Data.Nat.proofIAmRewriting` to not get
conflicts!). Proving statements is not easy, but it gets better with practice,
and idris will try to help you as much as possible!

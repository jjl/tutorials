# category theory

> infixr 0 >->
> infixr 0 >+>

## Data structures

### monoid

## Morphisms / Arrows

Functions are the most straightforward example of a morphism. Given a function of type `a -> b` we
say it is a morphism from `a` to `b` and given that is it literally written with the arrow, you can
see how morphisms get their other name, "arrows". But morphisms are a broader idea encompassing the
general idea of a function - take one thing, return another thing.

An easy way to create a custom morphism is to create a data type that wraps a function:
  
< namespace Example
<  public export
>  data Example a b = MkExample (a -> b)
>
>  runExample : Example a b -> a -> b
>  runExample (MkExample f) = f

Note that while we provide a convenient runner function, the concept of morphism says nothing about
*how* a morphism is run and this can differ in practice.

### Precategory

In category theory, a precategory is a collection of:

* A set of objects of type `k` (for functions and `Example`, k is `Type`).
* For every two objects A and B in `k`, a set Mor(A,B) of the morphisms between them.
* For every three objects A, B and C in `k`, a binary operation composition:
  * Written `Mor(A,B) x Mor(B,C) -> Mor(A,C)`.
  * Where composition (here dot) obeys associativity: `a . (b . c) = (a . b) . c```

We choose to interpret this by defining an interface against a morphism. As a morphism is between
objects of a type `k`, choosing a morphism fixes `k` for us and composition can be a method:

> interface Precategory ((>->) : k -> k -> Type) where
>   compose : (b >-> c) -> (a >-> b) -> (a >-> c)

The suggestive arrow-shaped name here is intended to help show the not because it's funny but to
make it clear what is going on in the signature of `compose`. Given two morphisms with compatible
types, we can produce a combined morphism. Were we to choose to fix it to the regular function
arrow, we get `(b -> c) -> (a -> b) -> (a -> c)`, regular function composition.

To implement Precategory for functions is slightly tedious however, as you cannot curry the function
arrow. This is because idris does not know whether it means the dependent or non-dependent function
arrow. The workaround is simple, we define a function `Fun` as a non-dependent arrow.

> Fun : Type -> Type -> Type
> Fun x y = x -> y
>
> Precategory Fun where compose f g x = f (g x)

> Precategory Example where
>   compose (MkExample g) (MkExample f) = MkExample (g . f)

In functional programming, most implementations of `Precategory` are called `Semigroupoid` for
reasons... I think it's a spectacularly unhelpful name, but it's worth knowing.

### Category

In category theory, a category is a collection of:

* A precategory `C`
* For every two A and B of C's object type `k`, the set of identity morphisms:
  * Obeying the identity law: `compose id f = compose f id`

This is fairly simple to represent in code, though unfortunately we have to use a lowercase name to
keep the definition short for idris reasons. Pretend `c` is `>->` or some other arrow shaped name:

> interface Precategory c => Category (c : k -> k -> Type) where
>   id : a `c` a

Once again, it's simple to implement for functions and `Example`:

> Category Fun where
>   id x = x
> Category Example where
>   id = MkExample (\x => x)

### Endofunctors

We'll come back to functors in their full glory later, but for now, we'll limit ourselves to a
simpler kind of functor, the endofunctor. It's just a functor, except both of the arrows are the same:

### Adjoints

### Products

> data Product : (k -> k -> Type) -> (j -> j -> Type) -> (k, j) -> (k, j) -> Type where
>   MkProduct : {j, k : Type}
>            -> {l : k -> k -> Type}
>            -> {r : j -> j -> Type}
>            -> {x, y : (k, j)}
>            -> Product l r x y

> Precategory c => Precategory d => Precategory (Product c d) where
>   compose x y = ?aaa--(MkProduct {l,r}) (MkProduct {l=m,r=s}) = MkProduct (compose l m) (compose r s)

-- > Category c => Category d => Category (c, d) where
-- >   identity = (identity, identity)


### Functors and Endofunctors

Warning: do you know the haskell typeclass `Functor`? This is not that, it is more general!

In category theory, a functor `F` is a mapping between two categories `L` and `R` (left and right) such that:
* Every object `x` in `L` is mapped to an object `F x` in `R`.
* Every morphism `f : x -> y` in L is mapped to a morphism `F g : F x -> F y` obeying:
  * identity: `(F id) x = id (F x)`
  * composition: `F (g . f) = F g . F f`

Here's our attempt at making that into code (note: we misspell it because Idris already defines
`Functor` in the prelude and it's the haskell version):

> interface Funktor ((>->) : from -> from -> Type) ((>+>) : to -> to -> Type) (f : from -> to) where

The arrows are morphisms as before, but now there are two of them. There's also another parameter, a
function `from -> to`. Notice that `from` is the type the left arrow is a morphism over and `to` is
the type the right arrow is a morphism over.

>   fmap : (a >-> b) -> (f a >+> f b)

We can see `fmap` as a way of turning the negative arrow into the positive arrow. The function `f`
is used to convert `a` and `b` (of type `from`) into their equivalents in the plus arrow (of type `to`)

We'll come back to functors in their full glory later, but for now, we'll limit ourselves to a
simpler kind of functor, the endofunctor. It's just a functor, except both of the arrows are the same:

> interface Funktor c c f => Endo (c : k -> k -> Type) (f : k -> k) where
> (c : k -> k -> Type) => (f : k -> k) => Funktor c c f => Endo c f where -- Free implementation!

Now let's fix that arrow to be the function arrow: `fmap : (a -> b) -> (f a -> f b)`. If you know
haskell's functor, that should look pretty familiar (it's the same, it's just usually written
without the rightmost pair of parens, since `->` is right associative!)

We are still left with our `f : k -> k` parameter. In the case of function, `k : Type`, meaning that
`f : Type -> Type`, allowing us to change the types either side of the arrow.



-- ### Applicative

-- Applicatives are a kind of endofunctor.

-- > interface Endo c f => Applikative (c : k -> k -> Type) (f : k -> k) where
-- >   pure : c a (f a)
-- -- >   (<*>) : c (f (a -> b)) (c (f a) (f b))

-- > interface Applikative c f => Mando (c : k -> k -> Type) (f : k -> k) where
-- >   bind : c (f a) (c a (f b)) -> 
-- > interface Applicative c f => Monad (c : k -> k -> Type) (f : k -> k) where

## Pullbacks

in general a pullback of `f` and `g` takes:

* a morphism `f : X -> Z`
* a morphism `g : Y -> Z`

and produces:

* a morphism `p1 : P -> X`
* a morphism `p2 : P -> Y`

where:

* `p1 . f = p2 . g` (commutativity)

the degenerate case of a pullback is the independent product:

`P = (X,Y), p1 = fst, p2 = snd`

Note though that the definition of pullback does not mention the ability to make P given X and Y.

## Fibres

Given a morphism `f : X -> Y`, the fibre of an element `y : Y` under `f` is the set of `x : X` for
which `f x = y`.

If `f = isEven : Nat -> Bool`:

* the fibre of `True` is the set of even naturals.
* the fibre of `False` is the set of odd naturals.

If `f = mod3 : Nat -> Nat`:

* the fibre of `Z` is the set of Naturals divisible by 3
* the fibre of `S Z` is the set of Naturals not divisible by 3 with a remainder 1
* the fibre of `S (S Z)` is the set of Naturals not divisible by 3 with a remainder 2

Or in general, the set of values that map onto a given output of a morphism. You can visualise these
like a thin fibre - if you attach it somewhere, all the points along the fiber are mapped to the
point where you have affixed it.

### Fibre bundles

given:
  f : X -> Y is functor
  y : Y

the subcategory of X consisting of
  objects x : X for which f x = y
  morphisms m for which f(m) = id^x


So every point along the fibre maps to a point in the base space. Or every point on the fibre knows
where it starts in base space. We say that the fibers map points on a 'total space' to a base space. We req

A fibre thus represents

 are not unique, thus we are focusing on 'surjective functions'.

Fibres on their own are not all that interesting. Usually for a given s


If we take all the fibres under `f` for all points (values) in the base space, we get a fibre
bundle under f.


## Recursion schemes

Catamorphism = fold
Anamorphism = unfold
Hylomorphism = cata + ana, fused: unfold a set of intermediate results, fold to a value
Metamorphism = cata + ana, unfused
Paramorphism = cata with additional access to the recursive onward subobjects + results

### and in category theory...

cata = unique homomorphism from an initial algebra into some other algebra
ana of a coinductive type = the assignment of a coalgebra to its unique moprhism to the final coalgebra of an endofunctor

# dictionary

morphism
: function
